package com.mscodes.springsecurityjwt.constants;

public enum MediaTypes {

    IMAGE("Image", "JPEG,PNG,JPG"),
    VIDEO("Video", "MP4,MKV,WAV"),
    AUDIO("Audio","MP3,WAV");

    private String mediaType;
    private String fileType;

    MediaTypes(String mediaType, String fileType) {
        this.mediaType = mediaType;
        this.fileType  = fileType;
    }
}
