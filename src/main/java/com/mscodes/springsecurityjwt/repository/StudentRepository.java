package com.mscodes.springsecurityjwt.repository;

import com.mscodes.springsecurityjwt.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student,Integer> {

    @Query(value = "SELECT * FROM STUDENT WHERE first_name =:name",nativeQuery = true)
    public List<Student> getStudents(String name);
}
